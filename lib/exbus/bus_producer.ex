defmodule BusProducer do
  @moduledoc """
  The BusProducer is the Bus data producer
  """

  use GenStage

  require Logger

  def start_link(_args) do
    GenStage.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    {:ok, connection} = AMQP.Connection.open()
    {:ok, channel} = AMQP.Channel.open(connection)

    scheduled_bus_depart()
    {:producer, %{channel: channel, count: 0, stop_number: 0, bus_stops: bus_stop_list()}}
  end

  def handle_info(:go, state) do
    Logger.info "Bus departure >> "

    IO.inspect(state)

    next_bus_stop()
    {:noreply, [], state}
  end

  def handle_info(:bus_stop, state) do
    Logger.info "Bus stop >> "

    with count <- Map.get(state, :count),
         stop_number <- Map.get(state, :stop_number),
         bus_stops <- Map.get(state, :bus_stops),
         channel <- Map.get(state, :channel),
         stop_name <- Map.get(bus_stops, stop_number),
         up <- Enum.random(0..10),
         down <- Enum.random(0..10),
         new_count <- count + up - down
    do
      AMQP.Basic.publish(
        channel,
        "",
        "bus_queue",
        "#{stop_name}, #{up}, #{down}, #{new_count}"
      )
      next_bus_stop()
      {:noreply, [], %{state | count: new_count, stop_number: stop_number + 1}}
    end
  end

  defp bus_stop_list() do
    %{
      0	=> "Gare SNCF",
      1 => "Pl. de la Brèche",
      2 => "Montplaisir",
      3 =>	"Place des Marronniers",
      4 =>	"Rambaudières",
      5 => "Base de Loisirs",
      6 => "Av. François Mitterrand",
      7 => "Gare routière",
    }
  end

  defp scheduled_bus_depart() do
    Process.send_after(self(), :go, 5_000)
  end

  defp next_bus_stop() do
    Process.send_after(self(), :bus_stop, Enum.random(3_000..10_000))
  end
end
