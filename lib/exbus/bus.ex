defmodule Exbus.Bus do
  @moduledoc """
  The Bus is the main entry point for the Bus data.
  It provides functions to interact with Bus struct.
  """

  defstruct [
    :id,
    :number,
    :seats,
  ]

  @type t :: %__MODULE__{
    id: String.t(),
    number: integer(),
    seats: integer(),
  }

  @spec up(Exbus.Bus.t(), number) :: Exbus.Bus.t()
  def up(%__MODULE__{seats: seats} = bus, up) do
    %{bus | seats: seats + up}
  end

  @spec down(Exbus.Bus.t(), number) :: Exbus.Bus.t()
  def down(%__MODULE__{seats: seats} = bus, down) do
    %{bus | seats: seats - down}
  end

  def print(%__MODULE__{} = bus) do
    IO.puts "Bus #{bus.id} has #{bus.seats} seats"
  end
end
