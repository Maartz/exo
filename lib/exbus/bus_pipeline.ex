defmodule BusPipeline do
  use Broadway

  @producer BroadwayRabbitMQ.Producer

  @producer_config [
    queue: "bus_queue",
    declare: [durable: true],
    on_failure: :reject_and_requeue
  ]

  def start_link(_args) do
    options = [
      name: BusPipeline,
      producer: [
        module: {@producer, @producer_config},
        concurrency: 1
      ],
      processors: [
        default: [
          concurrency: System.schedulers_online() * 2
        ]
      ]
    ]

    Broadway.start_link(__MODULE__, options)
  end

  def handle_message(_processor, message, _context) do
    IO.inspect(message, label: "message")
  end

  defp prepare_messages(messages, _context) do
    messages =
      Enum.map(messages, fn message ->
        Broadway.Message.update_data(message, fn data ->
          [stop, ups, downs, customer_count] = String.split(data, ",")
          %{stop: stop, ups: ups, downs: downs, customer_count: customer_count}
        end)
      end)
  end
end
